#include <ACROBOTIC_SSD1306.h>

#include <ESP8266WiFi.h>
//#include <WifiClient.h>
#include <DHT.h>
#include <Wire.h>

#define DHTPIN 2 
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

const char WiFiSSID[] = "SSID";
const char WiFiPSK[] = "PSK";
String deviceName = "deviceName";
const int WIFI_LED = 0;

void setup() {
  initHardware();
  connectWiFi();

}

void connectWiFi(){
  byte ledStatus = LOW;

  // Set WiFi mode to station (as opposed to AP or AP_STA)
  WiFi.mode(WIFI_STA);

  WiFi.begin(WiFiSSID, WiFiPSK);

  while (WiFi.status() != WL_CONNECTED){
    // Blink the LED
    digitalWrite(WIFI_LED, ledStatus); // Write LED high/low
    ledStatus = (ledStatus == HIGH) ? LOW : HIGH;

    delay(100);//delay for TCP stack maintenance and LED modulation
    }
}

void initHardware(){
  //could use serial port in the future though this is tricky 
  //with the ESP8266, especially if it's an ESP-01
  //Serial.begin(9600);
  pinMode(A0, INPUT);
  pinMode(WIFI_LED, OUTPUT);
  digitalWrite(WIFI_LED, LOW);
  dht.begin();
  Wire.begin();
  oled.init();
}

void drawUpdate(double aRead){
    oled.clearDisplay();
    oled.setTextXY(0,0);
    oled.putString("Measured input value:");
    oled.setTextXY(0, 10);
    oled.putString(String(aRead));
    
}

void powerSave(){
  //code to sleep in order to save power goes here
}

void loop() {
  powerSave();
  double aRead = analogRead(A0);
  drawUpdate(aRead);

}
